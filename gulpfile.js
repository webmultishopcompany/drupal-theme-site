var gulp = require('gulp');
var rename = require("gulp-rename");
var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer');
var babel = require("gulp-babel");

var paths = {
  styles: {
    src: 'scss/**/*.scss',
    dest: 'css/'
  },
  scripts: {
    src: 'js/**/*.es6',
    dest: 'js/'
  }
};

function styles() {
  return gulp.src(paths.styles.src)
    .pipe(sass())
    .pipe(sourcemaps.init())
    .pipe(postcss([ autoprefixer() ]))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(paths.styles.dest));
}

gulp.task('watch:scss', function() {
  return gulp.watch(paths.styles.src, styles);
});

function scripts() {
  return gulp.src(paths.scripts.src)
    .pipe(sourcemaps.init())
    .pipe(babel())
    .pipe(rename({
      extname: '.js'
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(paths.scripts.dest));
}

gulp.task('watch:es6', function() {
  return gulp.watch(paths.scripts.src, scripts);
});

gulp.task('watch', gulp.parallel('watch:scss', 'watch:es6'));

gulp.task('build', gulp.parallel(styles, scripts));